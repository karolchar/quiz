from django import forms
from .models import Quiz, Question, Answer


######Quiz form
class QuizCreationForm(forms.ModelForm):
    class Meta:
        model = Quiz
        fields = '__all__'


######Question form
class QuestionCreationForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['question_name', 'answer_quantity']


######Answer form
class AnswerCreationForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['answer_name', 'is_correct']
