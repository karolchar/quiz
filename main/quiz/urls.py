from django.urls import path
from .views import create_quiz, get_all_quizes, get_quiz, \
    create_question, get_question_in_quiz, \
    create_answer, check_answers_correctness, results

app_name = 'quiz'
urlpatterns = [
    path('quizes/create_quiz/', create_quiz, name='create_quiz'),
    path('quizes/', get_all_quizes, name="get_all_quizes"),
    path('quizes/<int:quiz_id>/', get_quiz, name='quiz'),
    path('quizes/<int:quiz_id>/<int:question_id>/', get_question_in_quiz, name='get_question_in_quiz'),
    path('quizes/<int:quiz_id>/<int:question_id>/check_correctness/', check_answers_correctness, name='check_answers_correctness'),
    path('quizes/<int:quiz_id>/create_question/', create_question, name='create_question'),
    path('quizes/create_answer/<int:quiz_id>/<int:question_id>/', create_answer, name='create_answer'),
    path('quizes/<int:quiz_id>/results/', results, name='results'),
]