from django.core.exceptions import ValidationError
from django.test import TestCase
from quiz.models import Quiz, Question
from quiz.stringgenerator import string_generator

class QuizTestCase(TestCase):
    def setUp(self):
        Quiz.objects.create(quiz_name=string_generator(150), question_quantity=0)
        Quiz.objects.create(quiz_name="    ", question_quantity=1000)

    def test_quiz_with_0_or_more_than_50_questions_raises_exception(self):
        quiz1 = Quiz.objects.get(id=1)
        quiz2 = Quiz.objects.get(id=2)
        self.assertRaises(ValidationError, quiz1.clean)
        self.assertRaises(ValidationError, quiz2.clean)

    def test_quiz_with_0_or_more_than_60_characters_raises_exception(self):
        quiz1 = Quiz.objects.get(id=1)
        quiz2 = Quiz.objects.get(id=2)
        self.assertRaises(ValidationError, quiz1.clean)
        self.assertRaises(ValidationError, quiz2.clean)