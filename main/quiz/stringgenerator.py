import random
import string


def string_generator(length):
    string_characters = string.printable

    created_string = ''.join(random.choice(string_characters) for i in range(length))

    return created_string
