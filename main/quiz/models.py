from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.
class Quiz(models.Model):
    quiz_name = models.CharField(max_length=60)
    question_quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return self.quiz_name

    class Meta:
        verbose_name_plural = 'quizes'

    #override validators on question_quantity field
    def clean(self):
        if self.question_quantity == 0:
            raise ValidationError(_("Quiz can be created with at least one question !"), code='Invalid_quantity')
        if self.question_quantity > 50:
            raise ValidationError(_("Quiz can be created with maximum of 50 questions !"), code='Invalid_quantity')


class Question(models.Model):
    question_name = models.CharField(max_length=255)
    question_number = models.IntegerField(default=0)
    answer_quantity = models.IntegerField(default=2)
    quiz = models.ForeignKey(
        Quiz,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.question_name

    #override validators on answer_quantity field
    def clean(self):
        if self.answer_quantity < 2:
            raise ValidationError(_("Question can be created with at least two answers !"))
        if self.answer_quantity > 5:
            raise ValidationError(_("Question can be created with maximum of five answers !"))


class Answer(models.Model):
    answer_name = models.CharField(max_length=255)
    is_correct = models.BooleanField(default=False)
    question = models.ForeignKey(
        Question,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.answer_name
