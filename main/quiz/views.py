from django.shortcuts import render
from django.urls import reverse

from .forms import QuestionCreationForm, QuizCreationForm, AnswerCreationForm
from django.http import HttpResponse, HttpResponseRedirect
from .models import Question, Quiz, Answer


# Create your views here.

###### Quiz
def get_all_quizes(request):
    quizes = Quiz.objects.all()
    return render(request, 'main.html', {'quizes': quizes})


def get_quiz(request, quiz_id):
    quiz = int(quiz_id)
    try:
        quiz = Quiz.objects.get(id=quiz)
    except Quiz.DoesNotExist:
        return HttpResponse('There is no such quiz #TODO html')
    #get all questions in quiz
    quiz.question_quantity = len(quiz.question_set.all())

    #register counters of correct and incorrect answers selected by particular user
    request.session["count_correct"] = 0
    request.session["count_incorrect"] = 0

    return render(request, 'crud/quiz/single_quiz.html', {'quiz': quiz, 'question_quantity': quiz.question_quantity})


def create_quiz(request):
    form = QuizCreationForm()
    if request.method == 'POST':
        form = QuizCreationForm(request.POST or None)
        if form.is_valid():
            quiz_name = form.cleaned_data['quiz_name']
            question_quantity = form.cleaned_data['question_quantity']
            quiz = Quiz(quiz_name=quiz_name, question_quantity=question_quantity)
            quiz.save()
            #declaration of counter for created questions in quiz
            request.session['count_questions_in_quiz'] = 0
            return HttpResponseRedirect(reverse('quiz:create_question', args=[quiz.id]))
        else:
            return render(request, 'crud/quiz/create_quiz.html', {'form': form})
    else:
        return render(request, 'crud/quiz/create_quiz.html', {'form': form})


###### Question
#view for rendering quiz upon clicking "start quiz"
def get_question_in_quiz(request, question_id, quiz_id):
    quiz_id = int(quiz_id)
    try:
        quiz = Quiz.objects.get(id=quiz_id)
    except Quiz.DoesNotExist:
        return HttpResponse('There is no such quiz #TODO html')

    next_or_submit = "Next"
    last_question_check = False
    if question_id == len(quiz.question_set.all()):
        last_question_check = True
        next_or_submit = "Submit"
    try:
        current_question = quiz.question_set.get(question_number=question_id)
    except Question.DoesNotExist:
        return HttpResponse('There is no such question #TODO html')

    #get all answers in particular question
    answers = current_question.answer_set.all()

    return render(request, 'crud/quiz/take_quiz.html',
                  {'quiz': quiz, 'current_question': current_question, 'answers': answers,
                   'next_or_submit': next_or_submit, 'is_last_question': last_question_check})


def create_question(request, quiz_id):
    quiz_id = int(quiz_id)
    try:
        quiz = Quiz.objects.get(id=quiz_id)
    except Quiz.DoesNotExist:
        return HttpResponse('There is no such quiz')
    form = QuestionCreationForm()

    #declaration of counter for answers in question
    request.session['count_answers_in_question'] = 0

    #check if number of created questions == amount of declared question quantity
    if request.session['count_questions_in_quiz'] == quiz.question_quantity:
        return HttpResponseRedirect(reverse('quiz:get_all_quizes'))

    if request.method == 'POST':
        form = QuestionCreationForm(request.POST or None)
        if form.is_valid():
            question_name = form.cleaned_data['question_name']
            answer_quantity = form.cleaned_data['answer_quantity']

            #increment counter before creating question in particular question so question number can be assigned
            request.session['count_questions_in_quiz'] += 1
            question = Question(quiz=quiz, question_name=question_name,
                                answer_quantity=answer_quantity, question_number=request.session['count_questions_in_quiz'])
            question.save()
            return HttpResponseRedirect(reverse('quiz:create_answer', args=[quiz.id, question.id]))
        else:
            return render(request, 'crud/question/create_question.html', {'form': form, 'quiz': quiz})
    else:
        return render(request, 'crud/question/create_question.html', {'form': form, 'quiz': quiz})


###### Answer
def create_answer(request, quiz_id, question_id):
    quiz_id = int(quiz_id)
    try:
        quiz = Quiz.objects.get(id=quiz_id)
        question = Question.objects.get(id=question_id)
    except (Quiz.DoesNotExist, Question.DoesNotExist):
        return HttpResponse('There is no such quiz or question')
    form = AnswerCreationForm()
    if request.method == 'POST':
        form = AnswerCreationForm(request.POST or None)
        if form.is_valid():
            answer_name = form.cleaned_data['answer_name']
            is_correct = form.cleaned_data['is_correct']
            answer = Answer(question=question, answer_name=answer_name, is_correct=is_correct)
            answer.save()

            #increment counter after creating answer in particular answer
            request.session['count_answers_in_question'] += 1
            if question_id == quiz.question_quantity:
                return HttpResponseRedirect(reverse('quiz:get_all_quizes'))
            if request.session['count_answers_in_question'] == question.answer_quantity:
                return HttpResponseRedirect(reverse('quiz:create_question', args=[quiz.id]))
            else:
                return HttpResponseRedirect(reverse('quiz:create_answer', args=[quiz.id, question.id]))
        else:
            return render(request, 'crud/answer/create_answer.html', {'form': form, 'quiz': quiz, 'question': question})
    else:
        return render(request, 'crud/answer/create_answer.html', {'form': form, 'quiz': quiz, 'question': question})


def check_answers_correctness(request, quiz_id, question_id):
    quiz_id = int(quiz_id)
    try:
        quiz = Quiz.objects.get(id=quiz_id)
    except Quiz.DoesNotExist:
        return HttpResponse('There is no such quiz')
    current_question = quiz.question_set.get(question_number=question_id)
    try:
        selected_answer = current_question.answer_set.get(pk=request.POST['answer'])
    except (KeyError, Answer.DoesNotExist):
        return render(request, 'crud/quiz/take_quiz.html', {'quiz': quiz, 'current_question': current_question})
    else:
        correct_answer = current_question.answer_set.get(is_correct=True)

        if selected_answer == correct_answer:
            request.session["count_correct"] += 1
        else:
            request.session["count_incorrect"] += 1

        if question_id == quiz.question_quantity:
            return HttpResponseRedirect(reverse('quiz:results', args=[quiz.id]))
        else:
            return HttpResponseRedirect(reverse('quiz:get_question_in_quiz', args=[quiz.id, question_id + 1]))


def results(request, quiz_id):
    quiz_id = int(quiz_id)
    try:
        quiz = Quiz.objects.get(id=quiz_id)
    except Quiz.DoesNotExist:
        return HttpResponse('There is no such quiz')

    count_correct = request.session["count_correct"]
    count_incorrect = request.session["count_incorrect"]

    total_questions = count_correct + count_incorrect

    return render(request, 'results.html',
                  {'quiz': quiz, 'total_questions': total_questions, 'count_correct': count_correct})
