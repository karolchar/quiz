# Quiz app

## Requires
```
Python 3.8
pip 19.0.1
```
## Check if python & pip are installed
```
In console:
python3 -V
pip -V
```
## Launching app
### \main
```
pip install requirements.txt

python manage.py runserver
```
### After server has started
```
http://127.0.0.1:8000/quiz/quizes/
```

### Termination of server
```
ctrl + C
```

